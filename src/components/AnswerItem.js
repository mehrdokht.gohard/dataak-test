import { useState, useEffect } from "react";
import placeholder from "../Images/profile-placeholder.png";

const AnswerItem = ({ answer }) => {
  const [answerUser, setAnswerUser] = useState([]);

  const dateParser = (d) => {
    if (!d) return "";
    return new Date(d).toLocaleString("fa-ir");
  };

  const fetchUser = async (userId) => {
    const res = await fetch(`http://localhost:5000/users/${userId}`);
    const data = await res.json();

    return data;
  };
  useEffect(() => {
    const getUser = async () => {
      if (!answer.userId) return;
      const user = await fetchUser(answer.userId);
      setAnswerUser(user);
    };

    getUser();
  }, []);
  return (
    <div class="shadow-lg flex flex-col rounded-md">
      <div
        class="shadow-lg flex flex-row-reverse rounded-md bg-white w-full"
        style={{ display: "flex", justifyContent: "space-between" }}
      >
        {" "}
        {answerUser && (
          <div class="flex text-right">
            <h3 class="p-2">{answerUser.name ?? ""} </h3>

            {answerUser.image && (
              <img
                class="m-2 h-8 w-8 rounded-full"
                src={answerUser.image}
                alt=""
              />
            )}
            {!answerUser.image && (
              <img class="m-2 h-8 w-8 rounded-full" src={placeholder} alt="" />
            )}
          </div>
        )}
        <div class="flex text-left">
          {" "}
          <h3 class="p-2">{dateParser(answer.createdAt)} </h3>
        </div>
      </div>

      <p class="flex h-24 flex-col text-right rounded-md p-4">{answer.body}</p>
    </div>
  );
};

export default AnswerItem;
