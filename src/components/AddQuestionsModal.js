import { useState } from "react";

const AddQuestion = ({ onAdd, setOpen }) => {
  const [title, setTitle] = useState("");
  const [text, setText] = useState("");
  const handleClose = () => setOpen(false);
  const onSubmit = (e) => {
    e.preventDefault();

    if (!text) {
      return;
    }

    onAdd({ text, title });

    setText("");
    setTitle("");
    setOpen(false);
  };

  return (
    <form onSubmit={onSubmit}>
      <div class="flex flex-col text-right ">
        <label class="mb-2">موضوع</label>
        <input
          class="w-full	h-8 rounded-md mb-4"
          type="title"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
      </div>
      <div class="flex flex-col text-right ">
        <label class="mb-2">متن سوال</label>
        <input
          class="w-full	h-16 rounded-md mb-4"
          type="text"
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
      </div>
      <div class="flex flex-row	">
        {" "}
        <input
          type="submit"
          value="ایجاد سوال"
          class="block  text-white bg-teal-600 border-teal-600  font-medium rounded-lg text-sm px-5 py-2.5 text-center "
        />
        <button
          class="block  text-teal-600   font-medium rounded-lg text-sm px-5 py-2.5 text-center "
          onClick={handleClose}
        >
          انصراف
        </button>
      </div>
    </form>
  );
};

export default AddQuestion;
