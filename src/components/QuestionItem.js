/* eslint-disable react-hooks/exhaustive-deps */
import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import placeholder from "../Images/profile-placeholder.png";

import moment from "jalali-moment";

const QuestionItem = ({ q, user }) => {
  const navigation = useNavigate();
  const onClickDetail = () => {
    navigation(`/questions/${q.id}/details`);
  };

  return (
    <div class="shadow-lg flex flex-col rounded-md">
      <div class="shadow-lg rounded-md flex bg-white h-12 flex-row-reverse text-right	">
        {user.image && (
          <img
            class="h-8
        w-8
        rounded-full
        m-2"
            src={user.image}
            alt=""
          />
        )}
        {!user.image && (
          <img
            class="h-8
       w-8
       rounded-full
       m-2"
            src={placeholder}
            alt=""
          />
        )}

        <h3 class="font-bold text-l px-2 mt-3">{q.title} </h3>
      </div>
      <p class="flex flex-col text-right bg-zinc-200 p-8 rounded-md">
        {q.text}
      </p>
      <div className="px-4 py-3 bg-zinc-200 text-left rounded-md sm:px-6">
        {" "}
        <button
          class="mt-3 w-full inline-flex justify-center rounded-md border border-teal-600 shadow-sm px-4 py-2 bg-zinc-200 text-base font-medium text-teal-600 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
          onClick={onClickDetail}
        >
          مشاهده جزییات
        </button>
      </div>
    </div>
  );
};

export default QuestionItem;
