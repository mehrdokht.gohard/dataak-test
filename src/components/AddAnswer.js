import * as React from "react";
import { useState, useEffect } from "react";
const AddAnswer = ({ questionId, answersList, setanswersList, ...props }) => {
  const [body, setBody] = useState("");
  console.log("questionId", questionId);

  const addAnswer = async (answer, questionId) => {
    answer.userId = 1;
    answer.createdAt = new Date().getTime();

    const res = await fetch(
      `http://localhost:5000/questions/${questionId}/answers`,
      {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify(answer),
      }
    );

    const data = await res.json();

    setanswersList([...answersList, data]);
  };

  const onSubmit = (e) => {
    e.preventDefault();

    if (!body) {
      return;
    }

    addAnswer({ body }, questionId);

    setBody("");
  };

  return (
    <form class="flex flex-col" onSubmit={onSubmit}>
      <div class="flex w-full flex-row-reverse px-14 font-bold text-xl">
        {" "}
        <h3>پاسخ خود را ثبت کنید</h3>
      </div>
      <div class="flex w-full flex-row-reverse px-14 py-4  ">
        <h3> پاسخ خود را بنویسید</h3>
      </div>
      <div class="flex w-full flex-row-reverse px-14">
        <input
          class="w-full	h-24 rounded-md mb-4 text-right flex justify-start "
          placeholder="...متن پاسخ "
          type="body"
          value={body}
          onChange={(e) => setBody(e.target.value)}
        />
      </div>
      <div class="flex flex-row-reverse">
        {" "}
        <input
          type="submit"
          value="ارسال پاسخ"
          class="flex mx-14  text-white bg-teal-600 border-teal-600  font-medium rounded-lg text-sm px-10 py-2.5 text-center "
        />
      </div>
    </form>
  );
};

export default AddAnswer;
