import * as React from "react";
import { Disclosure } from "@headlessui/react";
import AddQuestionsModal from "./AddQuestionsModal";
import Polygon from "../Images/Polygon.png";

const navigation = [
  { name: "Dashboard", href: "#", current: true },
  { name: "Team", href: "#", current: false },
  { name: "Projects", href: "#", current: false },
  { name: "Calendar", href: "#", current: false },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example({ onAdd, user, ...props }) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  console.log(user);
  return (
    <>
      <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8 ">
        <div className="relative flex items-center justify-between h-16 ">
          <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
            <div className="hidden sm:block sm:ml-6">
              <div className="flex space-x-4">
                <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                  <img src={Polygon} alt="" />

                  <div className=" p-1 text-gray-400  focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                    {user.name}
                  </div>

                  <div class="flex flex-row ">
                    <img
                      className="h-8 w-8 rounded-full"
                      src="https://media-exp1.licdn.com/dms/image/C4D03AQGGY2iACJC_PA/profile-displayphoto-shrink_800_800/0/1621354487857?e=1655942400&v=beta&t=wPg4KAbsTgcDTXblgTvQvGiLwdnhcjrRPYMrWSOea-g"
                      alt=""
                    />
                  </div>
                  <button
                    class="block ml-8 text-white bg-teal-600  font-medium rounded-lg text-sm px-5 py-2.5 text-center "
                    type="button"
                    data-modal-toggle="defaultModal"
                    onClick={handleOpen}
                  >
                    سوال جدید{" "}
                  </button>
                  {open ? (
                    <>
                      <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                        <div className="relative w-auto my-6 mx-auto max-w-3xl">
                          {/*content*/}
                          <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                            {/*header*/}
                            <div className="px-4 py-3 bg-white text-right w-96 rounded-md">
                              <h3 className="mt-3 w-full inline-flex  text-base font-large text-black  md:mt-0 md:ml-3 md:w-auto md:text-md font-bold">
                                ایجاد سوال جدید{" "}
                              </h3>
                              <button
                                className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                                onClick={() => setOpen(false)}
                              />
                            </div>
                            {/*body*/}
                            <div className="relative p-6 flex-auto bg-zinc-200 rounded-md">
                              <AddQuestionsModal
                                setOpen={setOpen}
                                onAdd={onAdd}
                              />
                            </div>
                            {/*footer*/}
                          </div>
                        </div>
                      </div>
                      <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                    </>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
          <h1 class="font-bold text-xl">لیست سوالات</h1>
        </div>
      </div>
    </>
  );
}
