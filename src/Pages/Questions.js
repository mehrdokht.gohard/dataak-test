import QuestionItem from "../components/QuestionItem";
import HeaderDetails from "../components/HeaderDetails";

const Questions = ({ QuestionsList, toggleLike, onAdd, user }) => {
  return (
    <>
      <HeaderDetails onAdd={onAdd} user={user} />

      {QuestionsList.map((q, index) => (
        <div class="flex flex-col px-16 py-8 bg-zinc-200 ">
          {" "}
          <QuestionItem key={index} q={q} user={user} />
        </div>
      ))}
    </>
  );
};

export default Questions;
