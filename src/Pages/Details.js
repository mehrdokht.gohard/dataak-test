/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect } from "react";
import { useParams } from "react-router";
import AddAnswer from "../components/AddAnswer";
import AnswerItem from "../components/AnswerItem";
import HeaderDetails from "../components/HeaderDetails";
import placeholder from "../Images/profile-placeholder.png";

const Details = ({ onAdd, user, ...props }) => {
  const [detailList, setdetailList] = useState([]);
  const [answersList, setanswersList] = useState([]);

  const { id } = useParams();

  const fetchDetail = async (id) => {
    const res = await fetch(`http://localhost:5000/QuestionsList/${id}`);
    const data = await res.json();

    return data;
  };
  const fetchAnswers = async (id) => {
    const res = await fetch(`http://localhost:5000/questions/${id}/answers`);
    const data = await res.json();

    return data;
  };
  useEffect(() => {
    const getDetail = async () => {
      const getDetailFromServer = await fetchDetail(id);
      setdetailList(getDetailFromServer);
    };
    getDetail();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const getAnswers = async () => {
      const getAnswersFromServer = await fetchAnswers(id);
      setanswersList(getAnswersFromServer);
    };
    getAnswers();
  }, []);

  return (
    <>
      <HeaderDetails onAdd={onAdd} user={user} />
      <div class=" bg-zinc-200 flex flex-col px-16 py-8 bg-zinc-200">
        <div class="flex flex-col px-16 py-8 bg-zinc-200">
          <div class="flex h-12 flex-row-reverse text-right rounded-md bg-white ">
            {user.image && (
              <img class="m-2 h-8 w-8 rounded-full" src={user.image} alt="" />
            )}
            {!user.image && (
              <img class="m-2 h-8 w-8 rounded-full" src={placeholder} alt="" />
            )}
            <h3 class="font-bold text-l px-2 mt-3">{detailList.title} </h3>
          </div>
          <div class="border-md">
            <p class="shadow-lg h-32 flex flex-col text-right rounded-md p-4">
              {detailList.details}
            </p>
          </div>
        </div>
        {answersList.map((answer, index) => (
          <div class="flex flex-col px-16 py-8 bg-zinc-200">
            {" "}
            <AnswerItem key={index} answer={answer} />
          </div>
        ))}
        <AddAnswer
          questionId={id}
          answersList={answersList}
          setanswersList={setanswersList}
        />
      </div>
    </>
  );
};

export default Details;
