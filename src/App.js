import { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import Questions from "./Pages/Questions";
import Details from "./Pages/Details";

const App = () => {
  const [QuestionsList, setQuestionsList] = useState([]);
  const [user, setUser] = useState([]);

  useEffect(() => {
    const getQuestion = async () => {
      const questionsFromServer = await fetchQuestions();
      setQuestionsList(questionsFromServer);
    };

    getQuestion();
  }, []);

  // Fetch Questions
  const fetchQuestions = async () => {
    const res = await fetch("http://localhost:5000/QuestionsList");
    const data = await res.json();

    return data;
  };

  const fetchUserInfo = async (userId) => {
    const res = await fetch(`http://localhost:5000/users/${userId}`);
    const data = await res.json();

    return data;
  };
  useEffect(() => {
    const getUserInfo = async () => {
      const user = await fetchUserInfo(1);
      setUser(user);
    };

    getUserInfo();
  }, []);

  // Add questions
  const addQuestion = async (question) => {
    question.userId = 1;
    const res = await fetch("http://localhost:5000/QuestionsList", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },

      body: JSON.stringify(question),
    });

    const data = await res.json();

    setQuestionsList([...QuestionsList, data]);
  };

  return (
    <Router>
      <div>
        <Routes>
          <Route
            path="/"
            element={
              <>
                {QuestionsList.length > 0 ? (
                  <Questions
                    user={user}
                    onAdd={addQuestion}
                    QuestionsList={QuestionsList}
                  />
                ) : (
                  "Please run the server"
                )}
              </>
            }
          />
          <Route
            path="/questions/:id/details"
            element={
              <Details
                QuestionsList={QuestionsList}
                user={user}
                onAdd={addQuestion}
              />
            }
          />
        </Routes>
      </div>
    </Router>
  );
};

export default App;
