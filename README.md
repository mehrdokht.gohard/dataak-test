# A few notes about the implementation

Like and dislike functions aren't included because the appropriate functionality would take to long to implement.
also the user is hard Coded because the authorization and authenication would need a login page and backend infrastruction.

This was my first time using tailwind, therefore it was a little bit difficult to change styles to it, so it might seem a little messy.

## To run the code you can use take following steps

### `yarn`

### `yarn start`

### `yarn run server`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `yarn run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
